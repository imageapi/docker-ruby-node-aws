FROM circleci/ruby:2.4

SHELL ["/bin/bash", "-c"]

RUN sudo apt-get update -qq --allow-releaseinfo-change && \
  sudo apt-get install -y --no-install-recommends unzip python-pip && \
  curl -sL https://deb.nodesource.com/setup_11.x | sudo -E bash - && \
  sudo apt-get install -y nodejs && \
  sudo npm install -g yarn && \
  sudo pip install awscli

RUN sudo mkdir -p /home/executor && \
  sudo useradd executor -u 999 -G circleci -s /bin/bash -d /home/executor && \
  echo "%circleci ALL=NOPASSWD: /usr/bin/docker" | sudo tee -a /etc/sudoers.d/50-circleci && \
  sudo chown -R executor:$(id -gn executor) /home/executor

WORKDIR /home/executor

ADD assume-role.sh .
RUN sudo echo "source /home/executor/assume-role.sh" >> ~/.bashrc
