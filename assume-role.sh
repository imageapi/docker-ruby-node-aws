#!/bin/bash

echo "Running setup scripts..."

if [ ! -n "$AWS_ACCESS_KEY_ID" ] && [ ! -n "$AWS_SECRET_ACCESS_KEY" ] && [ ! -n "$AWS_SESSION_TOKEN" ]
then
  if [ -n "$GLOBAL_ASSUME_ACCESS_ID" ] && [ -n "$GLOBAL_ASSUME_SECRET_KEY" ]
  then
    if [ -n "$DEPLOY_ROLE_ARN" ] && [ -n "$DEPLOY_ROLE_EXTERNAL_ID" ]
    then
      echo 'Found Global Assume and Deployment Identities. Attempting to assume deployment role...'
      echo "RoleArn: $DEPLOY_ROLE_ARN"
      echo ""

      # Assume the role using the global identity
      export AWS_ACCESS_KEY_ID="$GLOBAL_ASSUME_ACCESS_ID"
      export AWS_SECRET_ACCESS_KEY="$GLOBAL_ASSUME_SECRET_KEY"

      OUTPUT=`aws --output text \
        sts assume-role \
        --role-arn "$DEPLOY_ROLE_ARN" \
        --role-session-name bb-pipelines-deploy \
        --external-id "$DEPLOY_ROLE_EXTERNAL_ID"`

      export AWS_ACCESS_KEY_ID="`echo $OUTPUT | awk '{print $5}'`"
      export AWS_SECRET_ACCESS_KEY="`echo $OUTPUT | awk '{print $7}'`"
      export AWS_SESSION_TOKEN="`echo $OUTPUT | awk '{print $8}'`"

      # output new AWS identity
      aws --output table sts get-caller-identity
    else
      echo 'No Deloy Role found.'
    fi
  else
    echo 'No Global Assume Identity found.'
  fi
else
  echo 'Found AWS credentials in environment. Not attempting to assume role.'
fi
